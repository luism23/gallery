# Template for Nextjs

It is a template for Nextjs, for used in Startscoinc, used NextJs and Sass

## Install

Open terminal and clone repository [https://gitlab.com/franciscoblancojn/templatenextjs.git](https://gitlab.com/franciscoblancojn/templatenextjs.git)

```bash
git clone https://gitlab.com/franciscoblancojn/templatenextjs.git 
```

Rename Forlder

```bash
mv templatenextjs nameNewProyect
```

Move in Forlder

```bash
cd nameNewProyect
```

Remove git origin

```bash
git remote remove origin
```

Add your remote repositoy Git

```bash
git remote add origin <server>
```

Add your commit

```bash
git add .
git commit -m "Initial Commit"
```

Push in your Repository

```bash
git push
```

Install dependencies

```bash
npm i
```

Run dev

```bash
npm run dev
```

## Structure

#### api
It is forder for connect nextjs with your api

#### components
It is forder for create your components for proyect

#### data
It is forder for save information static for pages

#### functions
It is forder for create your funcions for proyect

#### pages
It is forder for create your pages for proyect

#### server
It is forder for create your getServerSideProps and getStaticProps for proyect

#### styles
It is forder for save your style and style static for proyect

#### svg
It is forder for save your svg for proyect


## Developer

[Francisco Blanco](https://franciscoblanco.vercel.app/)