import CriptoEth from "@/svg/criptoEth"

export default {
    header: {
        title: "NFters",
        linkTitle: [
            {
                link: "/",
                title: "Marketplace",
            },
            {
                link: "/",
                title: "Resource",
            },
            {
                link: "/",
                title: "About",
            },
        ],
        img: "lupa.png",
        linkInput: [
            {
                link: "/",
                text: "Upload",
                className: "bg-purple-2 border-purple-2 border-style-solid border-4",
            },
            {
                link: "/",
                text: "  Wallet",
                className: "bg-transparent color-purple-2 border-purple-2 border-style-solid border-4",
            },


        ]
    },
    banner: {
        title: "Discover, and collect Digital Art  NFTs ",
        text: "Digital marketplace for crypto collectibles and non-fungible tokens (NFTs). Buy, Sell, and discover exclusive digital assets.",
        link: "/",
        textLink: "Explore Now",
        numberSpan: [
            {
                text: "98k+",
                subText: "Artwork",
            },
            {
                text: "12k+",
                subText: "Auction",
            },
            {
                text: "15k+",
                subText: "Artist",
            },
        ],
        img: "banner-img.png",
        titleImg: "Abstr Gradient NFT",
        imgUser: "user.png",
        textUser: "Arkhan17",


    },
    world: {
        title: "The amazing NFT art of the world here",
        contentInfo: [
            {
                img: "card.png",
                title: "Fast Transaction",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam etiam viverra tellus imperdiet.",
            },
            {
                img: "nivel.png",
                title: "Growth Transaction",
                text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam etiam viverra tellus",
            },
        ]
    },
    images: {
        placeBidPrincipal: {
            img: "img.png",
            user: "The Futr Abstr",
            userImg: "user.png",
            stock: "10 in the stock",
            cryptoText: "Highest Bid",
            crypto: (
                <>
                    <CriptoEth size={12} />
                    0.25 ETH
                </>
            ),
        },
        placeBid: [
            {
                img: "cuadrado-1.png",
                cryptoText: "The Futr Abstr",
                crypto: (
                    <>
                        <CriptoEth size={12} />
                        0.25 ETH
                    </>
                ),
                cryptoNumber: "1 of 8",
                link: "/",
                textLink: "Place a bid",
            },
            {
                img: "cuadrado-2.png",
                cryptoText: "The Futr Abstr",
                crypto: (
                    <>
                        <CriptoEth size={12} />
                        0.25 ETH
                    </>
                ),
                cryptoNumber: "1 of 8",
                link: "/",
                textLink: "Place a bid",
            },
            {
                img: "cuadrado-3.png",
                cryptoText: "The Futr Abstr",
                crypto: (
                    <>
                        <CriptoEth size={12} />
                        0.25 ETH
                    </>
                ),
                cryptoNumber: "1 of 8",
                link: "/",
                textLink: "Place a bid",
            },
        ],
        titleTop: "Top Collections over ",
        subTitleTop: "Last 7 days",
        topCollections: [
            {
                imgTop: "logo-1.png",
                cryptoText: "CryptoFunks",
                cryptoAcount: (
                    <>
                        <CriptoEth size={12} />
                        19,769.39
                    </>
                ),
                numberPercentage: "+26.52%",
                color: "color-greed-1",
            },
            {
                imgTop: "logo-2.png",
                cryptoText: "Cryptix",
                cryptoAcount: (
                    <>
                        <CriptoEth size={12} />
                        2,769.39
                    </>
                ),
                numberPercentage: "+10.52%",
                color: "color-greed-1",
            },
            {
                imgTop: "logo-3.png",
                cryptoText: "Frensware",
                cryptoAcount: (
                    <>
                        <CriptoEth size={12} />
                        9,232.39
                    </>
                ),
                numberPercentage: "+2.52%",
                color: "color-red-1",
            },
            {
                imgTop: "logo-4.png",
                cryptoText: "PunkArt",
                cryptoAcount: (
                    <>
                        <CriptoEth size={12} />
                        3,769.39
                    </>
                ),
                numberPercentage: "+1.52%",
                color: "color-red-1",
            },
            {
                imgTop: "logo-5.png",
                cryptoText: "Art Crypto",
                cryptoAcount: (
                    <>
                        <CriptoEth size={12} />
                        10,769.39
                    </>
                ),
                numberPercentage: "+2.52%",
                color: "color-greed-1",
            },

        ]
    },
    collection: {
        title: "Collection Featured NFTs",
        imgCollection: [
            {
                imgs: [

                    "img-collection.png",
                    "img-collection-1.png",
                    "img-collection-2.png",
                    "img-collection-3.png",

                ], 
                textCollection: "Amazing Collection",
                img: "user.png",
                textUser: "by Arkhan",
                textLink:"Total 54 Items"
            },
            {
                imgs: [

                    "img-collection.png",
                    "img-collection-1.png",
                    "img-collection-2.png",
                    "img-collection-3.png",

                ], 
                textCollection: "Amazing Collection",
                img: "user.png",
                textUser: "by Arkhan",
                textLink:"Total 54 Items"
            },
            {
                imgs: [

                    "img-collection.png",
                    "img-collection-1.png",
                    "img-collection-2.png",
                    "img-collection-3.png",

                ], 
                textCollection: "Amazing Collection",
                img: "user.png",
                textUser: "by Arkhan",
                textLink:"Total 54 Items"
            },
        ],
    },
    create:{
        title:"Create and sell your NFTs",
        text:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Facilisi ac phasellus placerat a pellentesque tellus sed egestas. Et tristique dictum sit tristique sed non. Lacinia lorem id consectetur pretium diam ut. Pellentesque eu sit blandit fringilla risus faucibus.",
        link:"/",
        textLink:"Sign Up Now",
        imgUser:[
            {
                img:"fondo-1.png",
                imgUser:"user-2.png",
            },
            {
                img:"fondo-2.png",
                imgUser:"woman.png",
            },
            {
                img:"fondo-3.png",
                imgUser:"woman.png",
            },
        ]
    },
    footer: {
        imgLogo: "footer-logo.png",
        title: "Copywright 2021 Dialeats.com ",
        linkRs: [
            {
                link: "/",
                img: "facebook.png",
            },
            {
                link: "/",
                img: "facebook.png",
            },
            {
                link: "/",
                img: "facebook.png",
            },
        ]


    }
}