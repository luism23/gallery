export default ({ size = 13 }) => (
    <svg width={size / 16 + "rem"} viewBox="0 0 13 22" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M12.671 11.4565L6.50004 15.3232L0.325043 11.4565L6.50004 0.589844L12.671 11.4565ZM6.50004 16.5648L0.325043 12.6982L6.50004 21.9232L12.675 12.6982L6.50004 16.5648Z" fill="currentColor" />
    </svg>
)