import Header from "@/components/header"
import Banner from "@/components/banner"
import World from "@/components/world"
import Images from "@/components/images"
import Collection from "@/components/collection"
import Create from "@/components/create"
import Footer from "@/components/footer"


import Info from "@/data/index"

const Index = () => {
    return (
        <>
            <Header {...Info.header}/>
            <Banner {...Info.banner}/>
            <World {...Info.world}/>
            <Images {...Info.images}/>
            <Collection {...Info.collection}/>
            <Create {...Info.create}/>
            <Footer {...Info.footer}/>
        </>
    )
}
export default Index