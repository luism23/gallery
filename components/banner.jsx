import Link from "next/link"

import Bg from "@/components/Bg"
import Img from "@/components/Img"


const NumberSpan = ({ text = "", subText = "", link = "" }) => {
    return (
        <>
            <div className="flex flex-column flex-nowrap m-r-31">
                <h3 className="color-black font-montserrat font-40 font-w-700">
                    {text}
                </h3>
                <h5 className="color-gray-1 font-montserrat font-20 font-w-500">
                    {subText}
                </h5>
            </div>
        </>
    )
}

const Index = ({ title = "", text = "", textLink = "", textUser = "", link = "", numberSpan = [], img = "", titleImg, imgUser = "" }) => {
    return (
        <>
            <div className=" flex m-t-91 pos-r width-p-100 height-vh-min-100">
                <div className="flex">
                    <div className="flex-6 ">
                        <div className="flex m-l-52">
                            <h1 className="font-montserrat font-w-700 font-40 color-black">
                                {title}
                            </h1>
                            <p className="font-20 font-montserrat color-gray-1 m-t-20 m-b-40">
                                {text}
                            </p>
                            <div className="flex width-p-100">
                                <Link href={link}>
                                    <a className="bg-purple-1 d-block width-220  border-radius-50 m-b-31 p-h-40 p-v-20 font-20 font-montserrat color-white">
                                        {textLink}
                                    </a>
                                </Link>
                            </div>

                            <div className="flex ">
                                {
                                    numberSpan.map((e, i) => {
                                        return (
                                            <NumberSpan
                                                key={i}
                                                {...e}
                                            />
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                    <div className="flex-6">
                        <Img
                            src={img}
                            classNameImg=" height-vh-max-90"
                        />
                    </div>
                </div>
            </div>
        </>
    )
}
export default Index