import Link from "next/link"

import Img from "@/components/Img"


const LinkRs = ({ img="", link = "", }) => {
    return (
        <>
            <div className="flex">
                <Link href={link}>
                    <img className="flex m-r-11" src={`/image/${img}`} alt="" />
                </Link>
            </div>
        </>
    )
}

const Index = ({title="",text="",linkRs=[] }) => {
    return (
        <>
            <div className="container bg-blue-dark flex p-h-15 p-v-32">
                <div className="flex-4">
                   

                </div>
                <div className="flex flex-8 text-center flex-align-center">
                    <span className="color-white font-14 font-montserrat text-center width-p-100">
                        {title}
                    </span>
                </div>
                <div className="flex flex-2 flex-align-center">
                    {
                      linkRs.map((e,i) => {
                          return(
                              <LinkRs
                              key={i}
                              {...e}
                              />
                          )
                      })
                    }
                </div>
            </div>
        </>
    )
}
export default Index