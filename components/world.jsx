
const ContentInfo = ({ img = "", title = "", text = "" }) => {
    return (
        <>
            <div className="flex width-362 width-p-max-100 m-r-auto flex-nowrap p-b-65">
                <div className="width-38">
                    <img className="" src={`/image/${img}`} alt="" />
                </div>
                <div className=" width-p-100 m-l-16">
                    <h3 className="m-b-12 color-black font-20 font-w-700  font-montserrat ">
                        {title}
                    </h3>
                    <p className="m-0 font-montserrat color-black font-14 ">
                        {text}
                    </p>
                </div>
            </div>
        </>
    )
}

const Index = ({ contentInfo = [], title }) => {
    return (
        <>
            <div className="container flex m-t-122">
                <div className="flex-4 width-385 width-p-max-100">
                    <h1>
                        {title}
                    </h1>
                </div>
                <div className="flex-8 flex p-xl-l-61">
                    {
                        contentInfo.map((e, i) => {
                            return (
                                <ContentInfo
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>
            </div>
        </>
    )
}
export default Index