import Link from "next/link"

import Img from "@/components/Img"

const ImgUser = ({ img = "", imgUser = "" }) => {
    return (
        <>
            <div className="imgC pos-r flex border-radius-12">
                <Img
                    src={img}
                    className="flex width-p-100 border-radius-12"
                    classNameImg="width-p-100 border-radius-12"
                />
                <img
                    style={{ transform: "translateX(50%) translateY(50%)" }}
                    className="pos-a bottom-0 right-0 z-index-1 border-radius-99"
                    src={`/image/${imgUser}`}
                    alt="" />
            </div>

        </>
    )
}


const Index = ({ imgUser = [], link = "", textLink = "", title = "", text = "" }) => {
    return (
        <>
            <div className="flex flex-gap-50 m-b-99">
                <div className="flex-6 flex-gap-50 m-r-auto">
                    <div className="collapImg2">
                        {
                            imgUser.map((e, i) => {
                                return (
                                    <ImgUser
                                        key={i}
                                        {...e}
                                    />
                                )
                            })
                        }
                    </div>
                </div>
                <div className="flex-6 flex-align-center flex flex-gap-50">
                    <div>
                        <h1 className="font-32 font-w-700 font-dm_sans p-b-38">
                            {title}
                        </h1>
                        <p className="font-18 font-w-700 font-dm_sans color-gray-1 p-b-45">
                            {text}
                        </p>
                        <Link href={link} >
                            <a className="bg-purple-1 p-v-20 p-h-40 color-white font-20 font-montserrat border-radius-50">
                                {textLink}
                            </a>
                        </Link>
                    </div>
                </div>

            </div>


        </>
    )
}
export default Index