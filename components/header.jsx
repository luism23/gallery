import Link from "next/link"


const LinkInput = ({link= "" ,text="",className=""}) => {
    return (
        <>
            <div className="flex m-auto">
                <Link href={link}>
                    <a className={`font-14 font-dm_sans font-w-700 width-110 m-l-10 p-h-22 text-center p-v-18 m-r-20 bg-purple-2 color-white border-radius-50 ${className}`}>
                        {text}
                    </a>
                </Link>
            </div>
        </>
    )
}


const LinkTitle = ({ title = "", link = "" }) => {
    return (
        <>
            <div className="m-auto">
                <Link href={link}>
                    <a className="font-16 font-dm_sans font-w-500 m-r-47 color-gray-1 ">
                        {title}
                    </a>
                </Link>
            </div>
        </>
    )
}



const Index = ({ title, linkTitle = [], linkInput = [],img }) => {
    return (
        <>
            <header className="container flex p-v-30 p-h-15">
                <div className="flex-2 m-auto">
                    <h1 className="font-montserrat font-w-700 font-32 color-purple-1 m-r-68">
                        {title}
                    </h1>
                </div>
                <div className="flex-4 flex ">
                    {
                        linkTitle.map((e, i) => {
                            return (
                                <LinkTitle
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>
                <div className="flex-6 flex ">
                    <div className="p-h-18 m-auto p-v-5 flex flex-align-center flex-justify-between border-1 border-style-solid border-radius-50 width-300 height-50">
                        <input className="bg-transparent color-white border-0 border-radius-50" type="search" placeholder="Search" />
                        <img className="" src={`/image/${img}`} alt="" />
                    </div>
                    {
                        linkInput.map((e, i) => {
                            return (
                                <LinkInput
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>
            </header>
        </>
    )
}
export default Index