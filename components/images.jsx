import Link from "next/link"

import Img from "@/components/Img"



const TopCollections = ({ imgTop, imgCrypto, cryptoAcount = "", cryptoText = "", title = "", subTitle = "",numberPercentage="",color="" }) => {
    return (
        <>
            <div className="flex-4 p-b-24">
                <div className="flex">
                    <div className="">
                        <img className=" p-r-17" src={`/image/${imgTop}`} alt="" />
                    </div>
                    <div className="flex flex-align-center p-r-25">
                        <div>
                            <h3 className="font-16 font-w-500 font-dm_sans color-black">
                                {cryptoText}
                            </h3>
                            <h2>
                                {cryptoAcount}
                            </h2>
                        </div>
                    </div>
                    <div className={` font-20 font-w-700 font-montserrat flex flex-align-center ${color}`}>
                        {numberPercentage }
                    </div>
                </div>
            </div>
        </>
    )
}

const PlaceBid = ({ cryptoText = "", crypto = "", cryptoNumber = "",link = "", textLink = "", img = "" }) => {
    return (
        <>
            <div className="flex m-b-41">
                <div className="flex m-r-20">
                    <img className="" src={`/image/${img}`} alt="" />
                </div>
                <div className="flex width-100 ">
                    <span className="font-20 font-w-700 font-dm_sans ">
                        {cryptoText}
                    </span>
                    <h4 className="color-greed-1 font-dm_sans font-12 p-5 font-w-700 border-style-solid flex flex-align-center">
                        {crypto}
                    </h4>
                    <h5 className="color-gray-1 font-14 font-dm_sans m-v-10">
                        {cryptoNumber}
                    </h5>
                    <div className="">
                    <Link href={link}>
                        <a className="bg-purple-1 border-radius-50 p-5 font-14 font-montserrat color-white ">{textLink}</a>
                    </Link>
                </div>
                </div>
                
            </div>
        </>
    )
}
const PlaceBidPrincipal = ({ img = "", user = "", userImg = "", stock = "", cryptoText = "", crypto = "" }) => {
    return (
        <>
            <Img
                src={img}
                classNameImg=""
            />
            <div className="flex m-t-20">
                <div className="userCripto flex flex-nowrap p-r-41">
                    <img className="width-50" src={`/image/${userImg}`} alt="" />
                    <div className="width-p-100 flex flex-column flex-nowrap">
                        <span className="font-dm_sans font-20 font-w-700">
                            {user}
                        </span>
                        <p className="color-gray-1 font-14 font-dm_sans">{stock}</p>
                    </div>
                </div>
                <div className="flex flex-column flex-nowrap">
                    <div className="color-gray-1 font-12 font-dm_sans ">
                        {cryptoText}
                    </div>
                    <div className="font-16 font-dm_sans font-w-700">
                        {crypto}
                    </div>
                </div>
            </div>
        </>
    )
}

const Index = ({ placeBidPrincipal = {}, placeBid = [], topCollections = [],titleTop="",subTitleTop=""}) => {
    return (
        <>
            <div className="container m-l-0 flex flex-gap-53" style={{ marginLeft: 0 }}>
                <div className="flex-4 flex-gap-53">
                    <PlaceBidPrincipal {...placeBidPrincipal}  />
                </div>

                <div className="flex-4 flex-gap-53  m-h-auto">
                    {
                        placeBid.map((e, i) => {
                            return (
                                <PlaceBid
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>
                <div className="flex-4 flex-gap-53">
                <div>
                    <h2 className="font-24 font-w-700 font-montserrat color-black m-b-5">
                        {titleTop}
                    </h2>
                    <h3 className="font-18 font-w-700 font-montserrat color-purple-1 m-b-32">
                        {subTitleTop}
                    </h3>
                </div>
                    {
                        topCollections.map((e, i) => {
                            return (
                                <TopCollections
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>


            </div>
        </>
    )
}
export default Index