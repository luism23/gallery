import Link from "next/link"

import Img from "@/components/Img"

const ImgCollection = ({ imgs = [], textUser = "", textCollection = "", img = "", textLink = "", color = "", link = "" }) => {
    return (
        <>
            <div className="flex-4 flex-gap-32">
                <div>
                    <div className="collapImg">
                        {
                            imgs.map((e, i) => {
                                return (
                                    <Img
                                        key={i}
                                        src={e}
                                        classNameImg=""
                                    />)
                            })
                        }
                    </div>
                    <div>
                        <span className="font-20 font-700 font-dm_sans">
                            {textCollection}
                        </span>
                        <div className="flex flex-align-center flex-justify-between">
                            <div className="flex flex-align-center">
                                <img className="font-14 font-500 font-montserrat" src={`/image/${img}`} alt="" />
                                <span className="">
                                    {textUser}
                                </span>
                            </div>
                            <Link href={link}>
                                <a className="p-v-7 p-h-11 font-11 font-w-700 font-dm_sans border-style-solid border-radius-50 border-purple-1 bg-transparent border-1 ">
                                    {textLink}
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

const Index = ({ title = "", imgCollection = [] }) => {
    return (
        <>
            <div className="container">
                <h1 className="font-32 font-w-700 font-montserrat p-b-55">
                    {title}
                </h1>
                <div className="flex flex-gap-32 flex-justify-between">
                    {
                        imgCollection.map((e, i) => {
                            return (
                                <ImgCollection
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>
            </div>
        </>
    )
}
export default Index